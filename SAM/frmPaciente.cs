﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SAM
{
    public partial class frmPaciente : Form
    {
        public frmPaciente()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnIngresarPaciente_Click(object sender, EventArgs e)
        {
            frmAgregarPaciente frm = new frmAgregarPaciente();
            frm.Show();
        }

        private void mnuActualizar_Click(object sender, EventArgs e)
        {
            frmAgregarPaciente frm = new frmAgregarPaciente();
            frm.Show();
        }
    }
}
