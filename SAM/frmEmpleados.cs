﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SAM
{
    public partial class frmEmpleados : Form
    {
        public frmEmpleados()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            frmAgregarEmpleado frm = new frmAgregarEmpleado();
            frm.Show();
        }

        private void mnuActualizar_Click(object sender, EventArgs e)
        {
            frmAgregarEmpleado frm = new frmAgregarEmpleado();
            frm.Show();
        }

        private void btnIngresarEmpleado_Click(object sender, EventArgs e)
        {
            frmAgregarEmpleado frm = new frmAgregarEmpleado();
            frm.Show();
        }
    }
}
