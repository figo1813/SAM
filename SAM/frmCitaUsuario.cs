﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SAM
{
    public partial class frmCitaUsuario : Form
    {
        public frmCitaUsuario()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmCitaUsuario_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'sAMDataSet.vista_horarios_medico' Puede moverla o quitarla según sea necesario.
            this.vista_horarios_medicoTableAdapter1.Fill(this.sAMDataSet.vista_horarios_medico);

        }
    }
}
