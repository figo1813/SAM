﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SAM
{
    public partial class frmAgendaMedico : Form
    {
        public frmAgendaMedico()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void mnuAtender_Click(object sender, EventArgs e)
        {
            frmAtencion frm = new frmAtencion();
            
            frm.Show();
        }
    }
}
