﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SAM
{
    public partial class Login : Form
    {
        private SqlConnection conector;
        private Conexion objcone = new Conexion();
        private SqlDataReader tabla;
        public Login()
        {
            InitializeComponent();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            frmRecuperar recuperador = new frmRecuperar();
            recuperador.Show();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
                if (txtUsuario.Text!="" || txtClave.Text!="")
                {

                conector = objcone.conectar("SAM");
                
                String cadena = "select * from USUARIO where CORREO ='" + txtUsuario.Text + "'";
             
                tabla = objcone.consulta(cadena, conector);

                if (tabla.Read())
                    {
                        if (txtClave.Text==tabla[1].ToString())
                        {
                            if (Int32.Parse(tabla[2].ToString())==3)
                            {
                                frmAdministrador frmadmi = new frmAdministrador();
                                frmadmi.Show();
                                this.Hide();
                            }
                            if (Int32.Parse(tabla[2].ToString()) == 1)
                            {
                                frmMedico frmmedi = new frmMedico();
                                frmmedi.Show();
                                this.Hide();
                            }
                            if (Int32.Parse(tabla[2].ToString()) == 2)
                            {
                                frmUsuario frm = new frmUsuario();
                                frm.Show();
                                this.Hide();
                            }
                            
                        }
                        else
                        {
                            
                        }
                    }
                    else
                    {

                    }
                }
                else
                {
                errorProvider1.SetError(btnIngresar,"Ingrese los campos");
                }
                
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
