﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SAM
{
    public partial class frmMedico : Form
    {
        public frmMedico()
        {
            InitializeComponent();
            //CODIGO PARA CAMBIAR EL COLOR DEL MDIPAREN
            MdiClient ctlMDI;
            // Loop through all of the form's controls looking
            // for the control of type MdiClient.
            foreach (Control ctl in this.Controls)
            {
                try
                {
                    // Attempt to cast the control to type MdiClient.
                    ctlMDI = (MdiClient)ctl;

                    // Set the BackColor of the MdiClient control.
                    ctlMDI.BackColor = this.BackColor;
                }
                catch (InvalidCastException exc)
                {
                    // Catch and ignore the error if casting failed.
                }
            }

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnPerfil_Click(object sender, EventArgs e)
        {
            frmPerfilMedico frm = new frmPerfilMedico();
            frm.MdiParent = this;
            frm.Show();
        }

        private void btnCitas_Click(object sender, EventArgs e)
        {
            frmAgendaMedico frm = new frmAgendaMedico();
            frm.MdiParent = this;
            frm.Show();
        }
    }
}
