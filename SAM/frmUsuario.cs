﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SAM
{
    public partial class frmUsuario : Form
    {
        public frmUsuario()
        {
            InitializeComponent();
            //CODIGO PARA CAMBIAR EL COLOR DEL MDIPAREN
            MdiClient ctlMDI;
            // Loop through all of the form's controls looking
            // for the control of type MdiClient.
            foreach (Control ctl in this.Controls)
            {
                try
                {
                    // Attempt to cast the control to type MdiClient.
                    ctlMDI = (MdiClient)ctl;

                    // Set the BackColor of the MdiClient control.
                    ctlMDI.BackColor = this.BackColor;
                }
                catch (InvalidCastException exc)
                {
                    // Catch and ignore the error if casting failed.
                }
            }

        }

        private void bunifuThinButton21_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnPerfil_Click(object sender, EventArgs e)
        {
            frmPerfilUsuario frm = new frmPerfilUsuario();
            frm.MdiParent = this;
            frm.Show();

        }

        private void btnCitas_Click(object sender, EventArgs e)
        {
            frmCitaUsuario frm = new frmCitaUsuario();
            frm.MdiParent = this;
            frm.Show();
         
        } 


        private void btnPendientes_Click(object sender, EventArgs e)
        {
            frmCitasPendientes frm = new frmCitasPendientes();
            frm.MdiParent = this;
            frm.Show();
        }

        private void frmUsuario_Load(object sender, EventArgs e)
        {
            
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
